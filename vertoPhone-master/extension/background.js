/**
 * Created by igor on 27.09.16.
 */

"use strict";

const VERTO_APP_NAME = 'GBC Phone',
    ERR_NOT_INSTALLED = new Error(`Not installed ${VERTO_APP_NAME}`),
    ERR_NOT_RUNING = new Error(`No running ${VERTO_APP_NAME}`),
    CONTEXT_MENU_ID = 'selectAndCallVerto'
    ;

class Extension {

    constructor () {
        this.port = null;
        this.vertoApplication = null;
        this.setConnectVerto();
        this.contextMenu = null;
        this.jiraUrl = null;
        chrome.contextMenus.onClicked.addListener((info) => {
            if (info.menuItemId == CONTEXT_MENU_ID) {
                var number = info.selectionText;
                this.onClickCallMenu(number)
            }
        });
        this.initCTITelephony();
    }

    get NOT_INSTALL_TITLE () {
        return `No install '${VERTO_APP_NAME}'`;
    }

    get NOT_INSTALL_MSG () {
        return `Please install '${VERTO_APP_NAME}'`;
    }

    get NOT_RUN_TITLE() {
        return `Is not running ${VERTO_APP_NAME}`;
    }

    get NOT_RUN_MSG() {
        return `Please enable ${VERTO_APP_NAME}`;
    }

    get NO_SETTINGS_TITLE () {
        return `No settings ${VERTO_APP_NAME}`;
    }
    get NO_SETTINGS_MSG () {
        return `Please set settings ${VERTO_APP_NAME}`;
    }


    createContextMenu () {
        this.contextMenu = chrome.contextMenus.create({
            id: CONTEXT_MENU_ID,
            type: 'normal',
            title: 'Call',
            contexts: ['selection']
        });
    }

    removeContextMenu () {
        if (this.contextMenu) {
            chrome.contextMenus.remove(CONTEXT_MENU_ID);
            this.contextMenu = null;
        }
    }

    setError (err) {
        console.error(err);
    }

    setVertoConnectError () {
        if (!this.checkInstall()) {
            this.changeIcon('error64.png');
        } else {
            this.changeIcon('exclamation64.png');
        }
        this.CTITelephony.setJiraPhoneIconOffline();
        this.connected = false;
        chrome.browserAction.setPopup({popup: `errorPopup.html`});
        this.removeContextMenu();
    }

    setVertoConnectOK () {
        this.changeIcon('call64.png');
        this.CTITelephony.setJiraPhoneIconOnline();
        chrome.browserAction.setPopup({popup: `popup.html`});
        this.createContextMenu();
        this.connected = true;
    }

    enableVerto () {
        chrome.management.setEnabled(ext.vertoApplication.id, true, this.runVerto.bind(this));
    }

    runVerto () {
        chrome.management.launchApp(ext.vertoApplication.id);
    }

    checkInstall () {
        return !!this.vertoApplication;
    }

    checkEnabled () {
        return ext.vertoApplication && ext.vertoApplication.enabled;
    }

    setConnectVerto () {
        this.port = null;
        if (this.timerReconnect) {
            clearTimeout(this.timerReconnect);
        }
        chrome.management.getAll((apps) => {
            for (let app of apps) {
                if (app.name === VERTO_APP_NAME) {
                    this.vertoApplication = app;
                    this.port = chrome.runtime.connect(app.id, {name: "vertoExtension"});
                    this.port.onMessage.addListener(this.onMessageVerto.bind(this));
                    this.port.onDisconnect.addListener((e) => {
                        this.setVertoConnectError();
                        this.timerReconnect = setTimeout( ()=> {
                            this.setConnectVerto();
                        }, 5000);
                    });
                    return;
                }
            }
            this.vertoApplication = null;
            this.setVertoConnectError();
            this.setError(ERR_NOT_INSTALLED);
        });
    }

    onMessageVerto (data) {
        if (!data)
            return;

        switch (data.action) {
            case "login":
                this.setVertoConnectOK();
                break;

            case "logout":
                this.setVertoConnectError();
                break;

            case "noLiveConnect":
                this.setVertoConnectError();
                break;
            case "phoneVisibilityChecked":
                if (data.data.visible) {
                    this.sendMethod('closePhone', {});
                } else {
                    this.runVerto();
                }
                break;
            case "checkLogin":
                if (!data.data.login) {
                    this.CTITelephony.setJiraPhoneIconOffline();
                }
                break;
        }
    }

    sendMethod (method, params) {
        if (!this.vertoApplication)
            return this.setError(ERR_NOT_INSTALLED);

        if (!this.checkEnabled())
            return this.setError(ERR_NOT_INSTALLED);


        console.debug(`try send ${method}`);
        this.port.postMessage({
            action: method,
            data: params
        });
    }

    onClickCallMenu (number) {

        this.makeCall(number);
    }

    onChangeActiveCalls (calls) {}

    changeIcon (iconName) {
        chrome.browserAction.setIcon({path: `images/${iconName}`});
    }


    makeCall (number) {

      console.log("tiketti soittaessa");

      console.log("luodaaan tiketti");

      console.log("avataan ikkuna");

    //  var tempDuration = Date.now() - d.createdOn;
    //  //var duration = millisToMinutesAndSeconds(tempDuration);
    //  var subject = "Phone call to: " + d.params.destination_number;
    //  var body = "CALL DETAILS" + "\n" + "Call from: " + d.params.caller_id_number + "\n" + "Call to: " + d.params.destination_number + "\n" + "Time of call: " + d.createdOn + "\n" + "Duration: " + tempDuration + "\n" + "Agent name: " + d.params.caller_id_name;
    //  $.ajax({
    //     url: 'https://d3v-gbc.zendesk.com/api/v2/tickets.json',
    //     beforeSend: function(xhr) {
    //       xhr.setRequestHeader("Authorization", "Bearer " + "d978f83c28989436f9f015c01dd858c10f28afa5ce42068f7ec26f0f2423b238");
    //     },
    //     contentType:'application/json',
    //     type: 'POST',
    //     data: JSON.stringify({"ticket": {"subject": subject , "comment": { "body": body }}})
    //   })
    //   .done(function(data) {
    //     console.log(data.ticket);
         //tikettiID = data.ticket.id;
    //   });


    //   $.ajax({
    //      url: 'https://d3v-gbc.zendesk.com/api/v2/tickets.json',
    //      beforeSend: function(xhr) {
    //        xhr.setRequestHeader("Authorization", "Bearer " + "d978f83c28989436f9f015c01dd858c10f28afa5ce42068f7ec26f0f2423b238");
    //      },
    //      contentType:'application/json',
    //      type: 'POST',
    //      data: JSON.stringify({"ticket": {"subject": subject , "comment": { "body": body }}})
    //    })
    //    .done(function(data) {
    //      console.log(data.ticket);
    //      tikettiID = data.ticket.id;
    //    });

        this.sendMethod('makeCall', {
            number: number
        });
        this.runVerto();
    }

    hangupCall () {

      console.log("puhelu päättyi, ID on ");
      console.log(tikettiID);


    }

    initCTITelephony () {
        this.CTITelephony = new Process();
        this.CTITelephony.init();

        chrome.extension.onRequest.addListener((request, sender, sendResponse) => {
            if (request.hasOwnProperty('checkLogin')) {
                this.sendMethod('checkLogin', {});
            }

            if (request.pageLoad && pref.get('enabled'))
                sendResponse({ parseDOM: true });

            if (request.hasOwnProperty('number')) {
                this.makeCall(request.number)
            }
            if (request.hasOwnProperty('action')) {
                switch(request.action) {
                    case 'showPhone':
                        chrome.windows.getAll({populate:true}, function(winData){
                            var tabs = [];
                            for (var i in winData) {
                                if (winData[i].focused === true) {
                                    var winTabs = winData[i].tabs;
                                    var totTabs = winTabs.length;
                                    for (var j=0; j<totTabs;j++) {
                                        tabs.push(winTabs[j].url);
                                    }
                                }
                            }
                            for (var i in tabs) {
                                var pos = tabs[i].indexOf('/jira');
                                if(pos !== -1) {
                                    this.jiraUrl = tabs[i];
                                    this.jiraUrl = this.jiraUrl.slice(0,pos);
                                    this.jiraUrl = this.jiraUrl + '/jira';
                                    localStorage.setItem('jiraUrl', this.jiraUrl);
                                    return this.jiraUrl;
                                }
                            }
                        });
                        var getUrl = localStorage.getItem('jiraUrl');
                        if (this.port) {
                            this.sendMethod('jiraUrl', {jiraUrl : getUrl});
                            this.sendMethod('checkVisible', {});
                        } else {
                            this.runVerto();
                        }
                        break;
                }
            }
        });

        window.setInterval(() => {
            this.CTITelephony.isPageComplete();
        }, 5000);
    }

    c2cEnabled () {
        return pref.get("enabled");
    }

    c2cToggle () {
        var ON = pref.get('enabled');
        console.log(ON);
        if (ON) {
            this.CTITelephony.disable();
        } else {
            this.CTITelephony.enable();
        }
    }
}

var pref = Preferences; // alias for the Preferences object
var tikettiID = "";
var ext = new Extension();
