/**
 * Created by igor on 19.09.16.
 */

"use strict";

angular
    .module('app.callService', [])
    .service('CallService', function ($window, $rootScope) {
    return {
        makeCall: function (number, params) {
          console.log("soitettiin");
          console.log(number);
            $window.vertoSession.makeCall(number, params);
            console.log("soitettiin");
            console.log(number);

        },
        screenShare: function (id, params) {
            $window.vertoSession.screenShare(id, params);
            console.log("share");
        },

        dropCall: function (id) {
            $window.vertoSession.dropCall(id);
            console.log("drop call");
        },

        answerCall: function (id, params) {
            $window.vertoSession.answerCall(id, params);
            console.log("vastaa");
        },

        getCallStream: function (id) {
            return $window.vertoSession.getCallStream(id);
            console.log("stream");
        },

        holdCall: function (id) {
            $window.vertoSession.holdCall(id);
            console.log("holdissa");
        },

        unholdCall: function (id) {
            $window.vertoSession.unholdCall(id);
            console.log("un hold");
        },

        toggleHold: function (id) {
            $window.vertoSession.toggleHold(id);
            console.log("togle hold");
        },

        toggleMute: function (id) {
            $window.vertoSession.toggleMute(id);
            console.log("togle mute");
        },

        dtmf: function (id, d) {
            $window.vertoSession.dtmf(id, d);
        },

        transfer: function (id, dest) {
            if (!dest) {
                return false;
            }

            $window.vertoSession.transfer(id, dest);
        },

        openVideo: function (id) {
            $window.vertoSession.openVideo(id);
            console.log("soitettiin");
        },

        openMenu: function (id, name) {
            $window.vertoSession.openMenu(id, name);
            console.log("avaa menu");
        },

        getLastCallNumber: function (id, key) {
            return $window.vertoSession.getLastCallNumber();
            console.log("get last nymber");
        }
    }
});
